<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register()
    {
        return view('pages.form');
    }

    public function send(Request $request)
    {
        $namaDepan = $request["firstname"];
        $namaBelakang = $request["lastname"];
        //dd($request->all());

        return view('pages.landing', compact('namaDepan', 'namaBelakang'));
    }
}
