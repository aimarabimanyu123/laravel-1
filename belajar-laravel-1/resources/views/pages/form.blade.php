<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>

    <form action="/welcome" method="POST">
        @csrf
        <label for="">First name:</label> <br> <br>
        <input type="text" name="firstname"> <br> <br>
        <label for="">Last name:</label> <br> <br>
        <input type="text" name="lastname"> <br> <br>
        <label for="">Gender:</label> <br> <br>
        <input type="radio" name="gender"> Male <br>
        <input type="radio" name="gender"> Female <br>
        <input type="radio" name="gender"> Other <br> <br>
        <label for="">Nationality:</label> <br> <br>
        <select name="natinality" id="">
            <option value="1">Indonesian</option>
            <option value="2">Malaysian</option>
            <option value="3">Singaporean</option>
            <option value="3">Australian</option>
        </select> <br> <br>
        <label for="">Language Spoken:</label> <br> <br>
        <input type="checkbox"> Bahasa Indonesia <br>
        <input type="checkbox"> English <br>
        <input type="checkbox"> Other <br> <br>
        <label for="">Bio:</label> <br> <br>
        <textarea name="" id="" cols="30" rows="10"></textarea> <br>
        <input type="submit" value="Sign Up">
    </form>
</body>
</html>